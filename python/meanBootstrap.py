#%%
import numpy as np
from scipy.stats import sem, t
from scipy import mean
from math import sqrt
import csv
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
from matplotlib import rc
import time

confidence1 = float(0.95)
confidence2 = float(0.99) # NOTE: LABELS WONT CHANGE!
alpha1 = float(0.05)
alpha2 = float(0.01)
bootstrapSteps = 5000

# VARIABLES
for x in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
    dagen = x

    stream = open("D:/GitHub/WisCom/Mined/GonnaUse/42000/combined" + str(dagen) + "day.csv", "r")
    reader = csv.reader(stream, delimiter=",")


    x = list(reader)
    stream.close()

    n = len(x[0])
    result = np.empty((len(x), n))
    for i in range(0, len(x)):
        for j1 in range(0, len(x[i])):
            result[i][j1] = float(x[i][j1])

    # INITIALIZE ARRAYS
    mean = np.empty(len(x))
    ciU1 = np.empty(len(x))
    ciL1 = np.empty(len(x))
    ciU2 = np.empty(len(x))
    ciL2 = np.empty(len(x))

    # CALCULATE MEAN AND CONFIDENCE INTERVALS
    for i in range(0, len(x)):

        mean[i] = np.mean(result[i])

        m = np.mean(result[i])
        n = len(result[i])
        bootstraps = np.zeros(bootstrapSteps)

        for j in range(0,bootstrapSteps):
            sample = np.random.choice(result[i] , size=n, replace=True)
            bootstraps[j] = np.mean(sample)
        bootstrapsSorted = np.sort(bootstraps)

        ciU1[i] = bootstrapsSorted[int(bootstrapSteps *      alpha1 / 2) ]
        ciL1[i] = bootstrapsSorted[int(bootstrapSteps * (1 - alpha1 / 2))]

        ciU2[i] = bootstrapsSorted[int(bootstrapSteps *      alpha2 / 2) ]
        ciL2[i] = bootstrapsSorted[int(bootstrapSteps * (1 - alpha2 / 2))]


    # PLOTTING:
    xval = [0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100]
    fig = plt.figure(figsize=(10,5))
    ax = plt.axes()
    ax.set_xlim(0, 100)
    print('new')
    print(mean)

    ax.xaxis.set_major_formatter(PercentFormatter())
    cplot2 = ax.fill_between(xval, ciU1, ciU2, facecolor='yellow', alpha=0.5, label = 'betrouwbaarheidsinterval $ \\alpha = $'+ " " + str(alpha2))
    ax.fill_between(xval, ciL1, ciL2, facecolor='yellow', alpha=0.5)
    cplot1 = ax.fill_between(xval, ciU1, ciL1, facecolor='blue', alpha=0.5, label = 'betrouwbaarheidsinterval $ \\alpha = $'+ " " + str(alpha1))
    meanplot = ax.plot(xval, mean, 'k-', label = 'gemiddelde')


    plt.legend(bbox_to_anchor=(0.01, 0.99), loc=2, borderaxespad=0.)
    plt.ylabel('Gemiddeld aantal zieke kinderen')
    plt.xlabel('Segregatiepercentage')
    plt.title('Aantal zieke kinderen tot en met dag ' + str(dagen))
    plt.savefig('dag' + str(dagen) + '.png', bbox_inches='tight')
    plt.show()

#%%
