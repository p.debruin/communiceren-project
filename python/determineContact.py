#%%
import numpy as np

#pins = float(0.008071900885)  # for 250 DC's and H0=5
#pout = float(0.00003089852965)# for 250 DC's and H0=5 (5+5 = 10)
#pins = float(0.006393444321)  # for 250 DC's and H0=4
#pout = float(0.0000247179071) # for 250 DC's and H0=4 (4+4 = 8)
#pins = float(0.0097851471017)  # for 250 DC's and H0=6
#pout = float(0.0000123580370759) # for 250 DC's and H0=2 (6+2 = 8)
#pins = float(0.00393786424)  # for 250 DC's and H0=2.5
#pout = float(0.0000061787895) # for 250 DC's and H0=1 (2.5+1 = 3.5) ABOVE IS FOR 14 DAYS, BELOW FOR CORRECT 6
#pins = float(0.0257858)  # for 250 DC's and H0=2.5 ->6?
#pout = float(0.000032131) # for 250 DC's and H0=1 -> 2? (2.5+1 = 3.5)
pins = float(0.036302616)  # for 250 DC's and H0=8.2714
pout = float(0.000056952672) # for 250 DC's and H0=3.5449 (+ = 11.8163)
kins = 50
kout = 249 * 50

chanceTotalins = float(0)
for i in range(1,6):
    chanceDay = float(pins * (1 - pins)**(i - 1))
    chanceTotalins = chanceTotalins + chanceDay
print(chanceTotalins)

chanceTotalout = float(0)
for i in range(1,6):
    chanceDay = float(pout * (1 - pout)**(i - 1))
    chanceTotalout = chanceTotalout + chanceDay
print(chanceTotalout)

print("endprecalc")

expectedIns = (kins - 1) * chanceTotalins
expectedOut = kout * chanceTotalout

expectedTotal = expectedIns + expectedOut

print(expectedIns)
print(expectedOut)
print(expectedTotal)
#%%
