﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infectieziekten {
    class Simulation {

        private readonly int numberOfChildrenPerDayCare, numberOfDayCares, numOfChildren;
        private readonly float vaccineEffectiveness, vaccinePercentage, segregationPercentage;
        private int step;

        public int sick, healty, exposed, recovered, vaccinated;
        public int totalAffected, daysUntilOutbreak;

        public Random random;

        public ChildDayCare[] childDayCares;

        public int[] affectedUntilDay;

        public Simulation(int numberOfChildrenPerDayCare, int numberOfDayCares, float vaccineEffectiveness, float vaccinePercentage, float segregationPercentage, int numberOfSimulationDays) {

            this.numberOfChildrenPerDayCare = numberOfChildrenPerDayCare;
            this.numberOfDayCares           = numberOfDayCares;
            this.vaccineEffectiveness       = vaccineEffectiveness;
            this.vaccinePercentage          = vaccinePercentage;
            this.segregationPercentage      = segregationPercentage;

            random = new Random();

            numOfChildren = numberOfDayCares * numberOfChildrenPerDayCare;

            int numOfAntiVacDayCares = (int)((1 - vaccinePercentage) * numberOfDayCares);
            //int extraInN = (int)((1 - vaccinePercentage) * segregationPercentage * numOfChildren); // No of nonvacc in vacc DC

            int noOfNinN = (int) ((segregationPercentage + (1 - vaccinePercentage) * (1 - segregationPercentage)) * (float) numOfAntiVacDayCares * numberOfChildrenPerDayCare);
            int noOfVinN = numOfAntiVacDayCares * numberOfChildrenPerDayCare - noOfNinN;
            int noOfVaccine = (int) (vaccinePercentage * numOfChildren);
            int noOfVinV = noOfVaccine - noOfVinN;
            int noOfNinV = numOfChildren - noOfVaccine - noOfNinN;

            // Or daycares will not accept
            int noOfRefusingDCs = (int) (vaccinePercentage * numberOfDayCares * segregationPercentage);
            numOfAntiVacDayCares = numberOfDayCares - noOfRefusingDCs;
            int noOfV = (int) (vaccinePercentage * numOfChildren);
            noOfVinV = noOfRefusingDCs * numberOfChildrenPerDayCare;
            noOfNinV = 0;
            noOfVinN = noOfV - noOfVinV;
            noOfNinN = numOfChildren - noOfNinV - noOfVinN - noOfVinV;


            childDayCares = new ChildDayCare[numberOfDayCares];
            int childID = 0;

            int vinv = 0;
            int ninv = 0;
            int vinn = 0;
            int ninn = 0;

            totalAffected = 0;
            daysUntilOutbreak = -1;
            affectedUntilDay = new int[numberOfSimulationDays];

            for(int dayCareID = 0; dayCareID < numberOfDayCares; dayCareID++) {

                Child[] children = new Child[numberOfChildrenPerDayCare];
                
                // Antivac
                if (dayCareID < numOfAntiVacDayCares) {
                    
                    for (int j = 0; j < children.Length; j++) {

                        int age = (int)((MainWindow.maxDaysOldDayCare - MainWindow.minDaysOldDayCare) * random.NextDouble() + MainWindow.minDaysOldDayCare);
                        bool nonVaccine = random.NextDouble() <= (noOfNinN) / ((float) noOfNinN + noOfVinN);
                        bool workingVaccine = random.NextDouble() <= vaccineEffectiveness;

                        children[j] = new Child(dayCareID, childID, age, State.Susceptible, nonVaccine);
                        if (age > MainWindow.firstVaccineDays) children[j].Vaccinate(workingVaccine);
                        if (nonVaccine) ninn++; else vinn++;
                        childID++;
                    }

                    childDayCares[dayCareID] = new ChildDayCare(numberOfChildrenPerDayCare, children, true); // Nonvac

                } else { // Not antivac

                    for (int j = 0; j < children.Length; j++) {

                        int age = (int)((MainWindow.maxDaysOldDayCare - MainWindow.minDaysOldDayCare) * random.NextDouble() + MainWindow.minDaysOldDayCare);
                        bool nonVaccine = random.NextDouble() <= (noOfNinV) / ((float)noOfNinV + noOfVinV);
                        bool workingVaccine = random.NextDouble() <= vaccineEffectiveness;

                        children[j] = new Child(dayCareID, childID, age, State.Susceptible, nonVaccine);
                        if (age > MainWindow.firstVaccineDays) children[j].Vaccinate(workingVaccine);
                        if (nonVaccine) ninv++; else vinv++;
                        childID++;
                    }

                    childDayCares[dayCareID] = new ChildDayCare(numberOfChildrenPerDayCare, children, false); // Vaccine
                }
			}

            Console.WriteLine("ninn: " + ninn + " vinn: " + vinn + " ninv: " + ninv + " vinv: " + vinv);
            Console.WriteLine("n%: " + vinn / (float) (ninn + vinn) + " v%: " + vinv / (float)(ninv + vinv));
            Console.WriteLine((vinn + vinv) / ((float)ninn + vinn + ninv + vinv));
            if (false) { };

            // Fill contact patterns

            // TODO: Let the user change these values
            /*double percentageInsideContacts = 0.50d;
			double insideContact  = 0.01d;
			double outsideContact = 0.005d;
            double R0 = 17;

			contactPatterns = new double[numOfChildren, numOfChildren];

			// Contact patterns inside the daycare
			foreach (ChildDayCare childDayCare in childDayCares) {
				foreach (Child child1 in childDayCare.children) {
					foreach (Child child2 in childDayCare.children) {
						// Just connect everyone with constant rate
						contactPatterns[child1.childID, child2.childID] = insideContact;
					}
				}
			}

			// Note every child has now `numberOfChildrenPerDayCare - 1` contacts
            
			int averageOutsideContactsPerChild = (int) Math.Round(((numberOfChildrenPerDayCare - 1) / percentageInsideContacts) * (1 - percentageInsideContacts));
			int contactsLeft = (averageOutsideContactsPerChild * numOfChildren) / 2;

			// Fill Contact Patterns
			// TODO: still for testing (might have to change numbers + generation)
			while (contactsLeft > 0) {
				// Pick a random child
				int i = random.Next(numberOfDayCares);
				int x = random.Next(childDayCares[i].size);

				// Pick another random child
				// TODO: Prefer child with same `nonVaccine` state
				int j = random.Next(numberOfDayCares);
				int y = random.Next(childDayCares[j].size);

				// Try again when we have the same daycare or contact is already made.
				if (i == j || contactPatterns[childDayCares[i].children[x].childID, childDayCares[j].children[y].childID] > 0) continue;

				// Make contact
				contactPatterns[childDayCares[i].children[x].childID, childDayCares[j].children[y].childID] = outsideContact;
				contactPatterns[childDayCares[j].children[y].childID, childDayCares[i].children[x].childID] = outsideContact;

				contactsLeft--;
			}
            */

            // Infect child
            // NOTE: Uniformly distributed over all DayCares such that a child in a potential bigger DayCare has less change to be initially infected.
            bool infected = false;
            while (infected == false) {
                int randomDayCareID = random.Next(numberOfDayCares);
                int randomChildInDayCareID = random.Next(childDayCares[randomDayCareID].size);
                if(childDayCares[randomDayCareID].children[randomChildInDayCareID].state == State.Susceptible) {
                    childDayCares[randomDayCareID].children[randomChildInDayCareID].state = State.Infected;
                    infected = true;
                }
            }

            count();

            step = 0;
        }

        public void performStep() {

            // Refresh children
            for (int i = 0; i < numberOfDayCares; i++) {
                for (int j = 0; j < childDayCares[i].size; j++) {

                    // Child too old: Replacing children can cause negative effects: the ID can for instance switch from Infected to Suspectible?
                    if (childDayCares[i].children[j].age > MainWindow.maxDaysOldDayCare) {
                        bool nonVaccine = random.NextDouble() >= vaccinePercentage;
                        childDayCares[i].children[j] = new Child(i, childDayCares[i].children[j].childID, MainWindow.minDaysOldDayCare, State.Susceptible, nonVaccine);
                    }
                }
            }

            // Fill state array
            State[,] states = new State[numberOfDayCares, numberOfChildrenPerDayCare];
            for (int i = 0; i < numberOfDayCares; i++) {
                for (int j = 0; j < childDayCares[i].size; j++) {
                    states[i, j] = childDayCares[i].children[j].state;
                }
            }

            // State switches
            for (int i = 0; i < numberOfDayCares; i++) {
                for (int j = 0; j < childDayCares[i].size; j++) {

                    // S-> V
                    if(childDayCares[i].children[j].age == MainWindow.firstVaccineDays && !childDayCares[i].children[j].nonVaccine) {
                        bool workingVaccine = random.NextDouble() < vaccineEffectiveness;
                        childDayCares[i].children[j].Vaccinate(workingVaccine);
                    }

					// S-> E
					if (states[i, j] == State.Susceptible) {
						// Check each infected child
						for (int x = 0; x < numberOfDayCares; x++) {
							for (int y = 0; y < numberOfChildrenPerDayCare; y++) {
								if (states[x, y] == State.Infected) {
									double contact = implicitContact(i, x);
									if (random.NextDouble() < contact) {
										childDayCares[i].children[j].state = State.Exposed;
										childDayCares[i].children[j].lastStateChange = step; // <- Might want to do this with clever get / set
									}
								}
							}
						}
					}

                    // E -> I
                    if (states[i, j] == State.Exposed && step >= childDayCares[i].children[j].lastStateChange + MainWindow.daysFromExposedToInfected) {
                        childDayCares[i].children[j].state = State.Infected;
                        childDayCares[i].children[j].lastStateChange = step;
                        totalAffected += 1;
                    }

                    // I -> R
                    if (states[i ,j] == State.Infected && step >= childDayCares[i].children[j].lastStateChange + MainWindow.daysFromInfectedToRecovered) {
                        childDayCares[i].children[j].state = State.Recovered;
                        childDayCares[i].children[j].lastStateChange = step;
                    }

                    childDayCares[i].children[j].age++;
                }                
            }

            if (totalAffected >= 100 && daysUntilOutbreak == -1) daysUntilOutbreak = step;
            affectedUntilDay[step] = totalAffected;
            count();

            step++;
        }

        public void count() {

            sick = 0;
            healty = 0;
            exposed = 0;
            recovered = 0;
            vaccinated = 0;
            for (int i = 0; i < numberOfDayCares; i++) {
                for (int j = 0; j < childDayCares[i].size; j++) {
                    switch (childDayCares[i].children[j].state) {
                        case State.Susceptible:
                            healty++;
                            break;
                        case State.Vaccinated:
                            vaccinated++;
                            break;
                        case State.Exposed:
                            exposed++;
                            break;
                        case State.Infected:
                            sick++;
                            break;
                        case State.Recovered:
                            recovered++;
                            break;
                    }
                }
            }

        }

        double implicitContact(int dayCareID1, int dayCareID2) {
            if (dayCareID1 == dayCareID2) return MainWindow.insideContact;
            else return MainWindow.outsideContact;
        }
    }
}
