﻿using System.Collections.Generic;

namespace Infectieziekten {

	enum State {
		Susceptible,
		Vaccinated,
		Exposed,
		Infected,
		Recovered
	}

	struct Child {

		public          int   age;
		public          State state;
        public readonly int   dayCareID;
        public readonly int   childID;
        public          int   lastStateChange;

		/// <summary>
		/// Tells whether or not the child has a chance of getting vaccinated
		/// </summary>
		public bool nonVaccine;
        public bool workingVaccine {
            get => (state == State.Vaccinated);
        }

		public Dictionary<Child, double> contacts;

		// Maybe also link to kinderdagverblijf?

		public Child(int dayCareID, int childID, int age, State state, bool nonVaccine) {
			this.age            = age;
			this.state          = state;
			this.nonVaccine     = nonVaccine;
            this.dayCareID      = dayCareID;
            this.childID        = childID;

            lastStateChange = 0;
			contacts = new Dictionary<Child, double>();
		}

        public void Vaccinate(bool vaccineWorking) { // Maybe dont give bool as parameter but didnt know how to elegantly use Random Class of Simulation here
            if (nonVaccine)     return;
            if (vaccineWorking && state == State.Susceptible) state = State.Vaccinated;
        }
	}

	struct ChildDayCare {

		public int size;
		/// <summary>
		/// Whether or not we allow childs that for sure won't get vaccinated
		/// </summary>
		public bool nonVaccine;

        public Child[] children;

		public ChildDayCare(int size, Child[] children, bool nonVaccine) {
			this.size       = size;
			this.nonVaccine = nonVaccine;
            this.children   = children;
		}
	}
}