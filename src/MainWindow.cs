﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Windows.Forms.DataVisualization.Charting;
using System.Globalization;

namespace Infectieziekten {

	public partial class MainWindow : Form {

        Simulation simulation;

        private int numberOfChildrenPerDayCare, numberOfDayCares, numberOfSimulationDays;
        private float vaccineEffectiveness, vaccinePercentage, segregationPercentage;

        // Magic numbers
        public static readonly int maxDaysOldDayCare = 1460;
        public static readonly int minDaysOldDayCare = 61;
        public static readonly float daysPerMonth = 30.4f;
        private readonly int reportPerNumberOfSteps = 1;
        public static readonly int firstVaccineDays = 274; //(30.4 times 9)
        internal static readonly int daysFromExposedToInfected = 7;
        internal static readonly int daysFromInfectedToRecovered = 6;
        int numOfSimulationsPerPercentage = 1;//70;
        int numOfSimulationsOfPercentage = 11;//21; // (0, 0.1, 0.2, ..., 1.0)
        public int minOutbreakSize = 100;
                  
        // See contactChances.py
        public static readonly double insideContact = 0.036302616d;
        public static readonly double outsideContact = 0.000056952672d;

        private Thread simulationThread;
        private Thread gatherDataThread;

        int[,] resultsCount;
        int[,] resultsDays10;
        int[,] resultsDays20;
        int[,] resultsDays30;
        int[,] resultsDays40;
        int[,] resultsDays50;
        int[,] resultsDays60;
        int[,] resultsDays70;
        int[,] resultsDays80;
        int[,] resultsDays90;
        int[,] resultsDays100; // Dont kill meh

        public MainWindow() {
			InitializeComponent();
        }

        private void startNewSimulationButton_Click(object sender, EventArgs e) {
            
            Form newSimulationVariablesForm = new NewSimulationVariablesForm(this);
            newSimulationVariablesForm.Show();
        }

        public void newStartingVariables(int numberOfChildrenPerDayCare, int numberOfDayCares, float vaccineEffectiveness, float vaccinePercentage, float segregationPercentage, int numberOfSimulationDays) {

            this.numberOfChildrenPerDayCare = numberOfChildrenPerDayCare;
            this.numberOfDayCares           = numberOfDayCares;
            this.vaccineEffectiveness       = vaccineEffectiveness;
            this.vaccinePercentage          = vaccinePercentage;
            this.segregationPercentage      = segregationPercentage;
            this.numberOfSimulationDays     = numberOfSimulationDays;

            Properties.Settings.Default.numberOfChildrenPerDayCareSetting = numberOfChildrenPerDayCare;
            Properties.Settings.Default.numberOfDayCaresSetting           = numberOfDayCares;
            Properties.Settings.Default.vaccineEffectivenessSetting       = vaccineEffectiveness;
            Properties.Settings.Default.vaccinePercentageSetting          = vaccinePercentage;
            Properties.Settings.Default.segregationPercentageSetting      = segregationPercentage;
            Properties.Settings.Default.numberOfSimulationDaysSetting     = numberOfSimulationDays;
            Properties.Settings.Default.Save();
        }

        public void startSimulation() {
            if(simulationThread != null) simulationThread.Abort();
            simulationThread = new Thread(initSimulation);
            simulationThread.Start();
        }

        private void initSimulation() {
            Console.WriteLine("Initializing simulation");
            Console.WriteLine("");
            Console.WriteLine("Settings: ");
            Console.WriteLine("#Children / DayCare:    " + numberOfChildrenPerDayCare);
            Console.WriteLine("#DayCares:              " + numberOfDayCares);
            Console.WriteLine("Vaccine-effectiveness:  " + vaccineEffectiveness);
            Console.WriteLine("Vaccine-percentage:     " + vaccinePercentage);
            Console.WriteLine("Segretation-percentage: " + segregationPercentage);
            Console.WriteLine("#Simulation Days:       " + numberOfSimulationDays);
            Console.WriteLine("");

            // Clear old graphs
            clearChart1();
            clearChart2();

            simulation = new Simulation(numberOfChildrenPerDayCare, numberOfDayCares, vaccineEffectiveness / 100, vaccinePercentage / 100, segregationPercentage / 100, numberOfSimulationDays);
            Console.WriteLine("Simulation initialised!");
            Console.WriteLine("");

            runSimulation();
        }

        private void runSimulation() {
            for(int day = 0; day < numberOfSimulationDays; day++) {
                if (day % reportPerNumberOfSteps == 0) {
                    Console.WriteLine("iteration " + day + " S: " + simulation.healty + " V: " + simulation.vaccinated + " E: " + simulation.exposed + " I: " + simulation.sick + " Total Affected: " + simulation.totalAffected);
                    updateChart1(simulation.healty, simulation.exposed, simulation.sick, simulation.recovered, simulation.vaccinated);
                    updateChart2(simulation.exposed, simulation.sick);
                }
                if (simulation.exposed + simulation.sick == 0) { Console.WriteLine("Outbreak stopped"); break; }
                simulation.performStep();
            }

			Console.WriteLine();
			Console.WriteLine("Simulation done: Saving data...");
            saveSimulationData();
        }

        private void gatherData() {

            resultsCount   = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays10  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays20  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays30  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays40  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays50  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays60  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays70  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays80  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays90  = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];
            resultsDays100 = new int[numOfSimulationsOfPercentage, numOfSimulationsPerPercentage];

            numberOfChildrenPerDayCare = 50;
            numberOfDayCares = 250;
            numberOfSimulationDays = 100;// (int) (daysPerMonth * 24);
            vaccineEffectiveness = 95f;
            vaccinePercentage = 93f;

            numberOfDayCares = 250; // CHANGING THIS? CONSULT PIETER!

            setProgressBarValue(progressBar1, 0);

            for (int i = 0; i < numOfSimulationsOfPercentage; i++) {

                setProgressBarValue(progressBar1, i);

                segregationPercentage = i / (numOfSimulationsOfPercentage - 1f) * 100f;

                setProgressBarValue(progressBar2, 0);

                Console.WriteLine("Simulating " + segregationPercentage + " percent of segregation");
                for (int j = 0; j < numOfSimulationsPerPercentage; j++) {
                    initSimulation();
                    resultsCount  [i, j] = simulation.totalAffected;
                    resultsDays10 [i, j] = simulation.affectedUntilDay[ 9];
                    resultsDays20 [i, j] = simulation.affectedUntilDay[19];
                    resultsDays30 [i, j] = simulation.affectedUntilDay[29];
                    resultsDays40 [i, j] = simulation.affectedUntilDay[39];
                    resultsDays50 [i, j] = simulation.affectedUntilDay[49];
                    resultsDays60 [i, j] = simulation.affectedUntilDay[59];
                    resultsDays70 [i, j] = simulation.affectedUntilDay[69];
                    resultsDays80 [i, j] = simulation.affectedUntilDay[79];
                    resultsDays90 [i, j] = simulation.affectedUntilDay[89];
                    resultsDays100[i, j] = simulation.affectedUntilDay[99];
                    Console.WriteLine("simulation done, affected: " + simulation.totalAffected);

                    setProgressBarValue(progressBar2, j);
                }

                setProgressBarValue(progressBar1, 11);
            }

            FileWriter.WriteMatrixToCSV(resultsCount  ,  "count-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays10 ,  "10day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays20 ,  "20day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays30 ,  "30day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays40 ,  "40day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays50 ,  "50day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays60 ,  "60day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays70 ,  "70day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays80 ,  "80day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays90 ,  "90day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
            FileWriter.WriteMatrixToCSV(resultsDays100, "100day-results-of-" + DateTime.Now.ToString("yyyy-MM-dd H-mm-ss") + ".csv");
        }

        delegate void setProgressBarValueCallback(ProgressBar progressBar, int value);

        private void setProgressBarValue(ProgressBar progressBar, int value) {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (chart.InvokeRequired) {
                setProgressBarValueCallback d = new setProgressBarValueCallback(setProgressBarValue);
                Invoke(d, new object[] { progressBar, value });
            } else {
                progressBar.Value = value;
            }
        }

        delegate void updateChart1Callback(int healty, int exposed, int sick, int recovered, int vaccinated);

        private void updateChart1(int healty, int exposed, int sick, int recovered, int vaccinated) {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (chart.InvokeRequired) {
                updateChart1Callback d = new updateChart1Callback(updateChart1);
                Invoke(d, new object[] { healty, exposed, sick, recovered, vaccinated });
            } else {
                chart.Series[0].Points.Add(healty);
                chart.Series[1].Points.Add(exposed);
                chart.Series[2].Points.Add(sick);
                chart.Series[3].Points.Add(recovered);
                chart.Series[4].Points.Add(vaccinated);
            }
        }

        delegate void clearChart1Callback();

        private void clearChart1() {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (chart.InvokeRequired) {
                clearChart1Callback d = new clearChart1Callback(clearChart1);
                Invoke(d, new object[] {});
            } else {
                foreach(Series serie in chart.Series) {
                    serie.Points.Clear();
                }
            }
        }

        delegate void updateChart2Callback(int exposed, int sick);

        private void updateChart2(int exposed, int sick) {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (chart.InvokeRequired) {
                updateChart2Callback callback = new updateChart2Callback(updateChart2);
                Invoke(callback, new object[] { exposed, sick });
            } else {
                chart2.Series[0].Points.Add(exposed);
                chart2.Series[1].Points.Add(sick);
            }
        }

        delegate void clearChart2Callback();

        private void gatherDataButton_Click(object sender, EventArgs e) {
            if (gatherDataThread != null) gatherDataThread.Abort();

            numOfSimulationsOfPercentage = (int) numericUpDown1.Value;
            numOfSimulationsPerPercentage = (int) numericUpDown2.Value;

            progressBar1.Maximum = numOfSimulationsOfPercentage;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            progressBar2.Maximum = numOfSimulationsPerPercentage;
            progressBar2.Step = 1;
            progressBar2.Value = 0;

            gatherDataThread = new Thread(gatherData);
            gatherDataThread.Start();
        }

        private void clearChart2() {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (chart2.InvokeRequired) {
                clearChart2Callback callback = new clearChart2Callback(clearChart2);
                Invoke(callback, new object[] { });
            } else {
                foreach (Series serie in chart2.Series) {
                    serie.Points.Clear();
                }
            }
        }

        private void saveSimulationData() {

            // Save states
            // TODO: Also save history (so 3D. 2D if indexed on childID's, then also save CayCareID)
            // Maybe dont even save states but different statistics
            string[,] states = new string[numberOfDayCares, numberOfChildrenPerDayCare];
            for(int i = 0; i < numberOfDayCares; i++) {
                for (int j = 0; j < numberOfChildrenPerDayCare; j++) {
                    switch (simulation.childDayCares[i].children[j].state) {
                        case State.Susceptible:
                            if(simulation.childDayCares[i].children[j].age > firstVaccineDays) {
                                states[i, j] = "N"; // Nonworking vaccine
                            } else {
                                states[i, j] = "S";
                            }                            
                            break;
                        case State.Vaccinated:
                            states[i, j] = "V";
                            break;
                        case State.Exposed:
                            states[i, j] = "E";
                            break;
                        case State.Infected:
                            states[i, j] = "I";
                            break;
                        case State.Recovered:
                            states[i, j] = "R";
                            break;
                    }
                }
            }
            FileWriter.WriteMatrixToCSV(states, "states.csv");
        }

        // From: https://stackoverflow.com/questions/24918768/progress-bar-in-console-application
        public static void drawTextProgressBar(int progress, int total, string processName) {
            
            // Draw empty progress bar
            Console.CursorLeft = 0;
            Console.Write("[");
            Console.CursorLeft = 32;
            Console.Write("]");
            Console.CursorLeft = 1;
            float onechunk = 30.0f / total;

            // Draw filled part
            int position = 1;
            for (int i = 0; i < onechunk * progress; i++) {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            // Print text
            Console.CursorLeft = 35;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(progress.ToString() + "/" + total.ToString() + " of " + processName); //blanks at the end remove any excess
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {

            if (simulationThread != null) simulationThread.Abort();
            if (gatherDataThread != null) gatherDataThread.Abort();
            base.OnFormClosing(e);
        }
    }
}
