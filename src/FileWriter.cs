﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infectieziekten {

    class FileWriter {

        public static void WriteMatrixToCSV<T>(T[,] dataMatrix, string fileName) {

            StreamWriter file = new StreamWriter(fileName);
            for (int i = 0; i < dataMatrix.GetLength(0); i++) {
                for (int j = 0; j < dataMatrix.GetLength(1); j++) {
                    file.Write(dataMatrix[i, j]);

                    file.Write(",");
                }
                file.Write("\n");

            }
            file.Close();
        }
    }
}