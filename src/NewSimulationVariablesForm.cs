﻿using System;
using System.Windows.Forms;

namespace Infectieziekten {
    public partial class NewSimulationVariablesForm : Form {

        MainWindow mainWindow;

        public NewSimulationVariablesForm(MainWindow mainWindow) {

            this.mainWindow = mainWindow;
            InitializeComponent();

            numberOfChildrenPerDayCareInput.Value =            Properties.Settings.Default.numberOfChildrenPerDayCareSetting;
            numberOfDayCaresInput.Value           =            Properties.Settings.Default.numberOfDayCaresSetting;
            vaccineEffectivenessInput.Value       = (decimal)  Properties.Settings.Default.vaccineEffectivenessSetting;
            vaccinePercentageInput.Value          = (decimal)  Properties.Settings.Default.vaccinePercentageSetting;
            segregationPercentageInput.Value      = (decimal)  Properties.Settings.Default.segregationPercentageSetting;
            numberOfSimulationMonthsInput.Value   = (decimal) (Properties.Settings.Default.numberOfChildrenPerDayCareSetting / 30.4f);
        }

        private void saveVariablesAndStartButton_Click(object sender, EventArgs e) {
            int   numberOfChildrenPerDayCare = (int)            numberOfChildrenPerDayCareInput.Value;
            int   numberOfDayCares           = (int)            numberOfDayCaresInput          .Value;
            float vaccineEffectiveness       = (float)          vaccineEffectivenessInput      .Value;
            float vaccinePercentage          = (float)          vaccinePercentageInput         .Value;
            float segregationPercentage      = (float)          segregationPercentageInput     .Value;
            int   numberOfSimulationDays     = (int)   ((float) numberOfSimulationMonthsInput  .Value * 30.4);

            mainWindow.newStartingVariables(numberOfChildrenPerDayCare, numberOfDayCares, vaccineEffectiveness, vaccinePercentage, segregationPercentage, numberOfSimulationDays);
            Close();
            mainWindow.startSimulation();            
        }
    }
}
