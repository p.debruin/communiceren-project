﻿namespace Infectieziekten {
    partial class NewSimulationVariablesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.numberOfChildrenPerDayCareInput = new System.Windows.Forms.NumericUpDown();
            this.numberOfDayCaresInput = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.vaccinePercentageInput = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.vaccineEffectivenessInput = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.saveVariablesAndStartButton = new System.Windows.Forms.Button();
            this.numberOfSimulationMonthsInput = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.segregationPercentageInput = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfChildrenPerDayCareInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfDayCaresInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vaccinePercentageInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vaccineEffectivenessInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfSimulationMonthsInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.segregationPercentageInput)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "#Children / DayCare";
            // 
            // numberOfChildrenPerDayCareInput
            // 
            this.numberOfChildrenPerDayCareInput.Location = new System.Drawing.Point(122, 11);
            this.numberOfChildrenPerDayCareInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numberOfChildrenPerDayCareInput.Name = "numberOfChildrenPerDayCareInput";
            this.numberOfChildrenPerDayCareInput.Size = new System.Drawing.Size(120, 20);
            this.numberOfChildrenPerDayCareInput.TabIndex = 1;
            this.numberOfChildrenPerDayCareInput.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numberOfDayCaresInput
            // 
            this.numberOfDayCaresInput.Location = new System.Drawing.Point(122, 37);
            this.numberOfDayCaresInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numberOfDayCaresInput.Name = "numberOfDayCaresInput";
            this.numberOfDayCaresInput.Size = new System.Drawing.Size(120, 20);
            this.numberOfDayCaresInput.TabIndex = 3;
            this.numberOfDayCaresInput.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "#DayCares";
            // 
            // vaccinePercentageInput
            // 
            this.vaccinePercentageInput.DecimalPlaces = 1;
            this.vaccinePercentageInput.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.vaccinePercentageInput.Location = new System.Drawing.Point(122, 87);
            this.vaccinePercentageInput.Name = "vaccinePercentageInput";
            this.vaccinePercentageInput.Size = new System.Drawing.Size(120, 20);
            this.vaccinePercentageInput.TabIndex = 7;
            this.vaccinePercentageInput.Value = new decimal(new int[] {
            93,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Vaccine-percentage";
            // 
            // vaccineEffectivenessInput
            // 
            this.vaccineEffectivenessInput.DecimalPlaces = 1;
            this.vaccineEffectivenessInput.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.vaccineEffectivenessInput.Location = new System.Drawing.Point(122, 61);
            this.vaccineEffectivenessInput.Name = "vaccineEffectivenessInput";
            this.vaccineEffectivenessInput.Size = new System.Drawing.Size(120, 20);
            this.vaccineEffectivenessInput.TabIndex = 5;
            this.vaccineEffectivenessInput.Value = new decimal(new int[] {
            95,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Vaccine-effectiveness";
            // 
            // saveVariablesAndStartButton
            // 
            this.saveVariablesAndStartButton.Location = new System.Drawing.Point(14, 165);
            this.saveVariablesAndStartButton.Name = "saveVariablesAndStartButton";
            this.saveVariablesAndStartButton.Size = new System.Drawing.Size(132, 23);
            this.saveVariablesAndStartButton.TabIndex = 8;
            this.saveVariablesAndStartButton.Text = "Save variables and start";
            this.saveVariablesAndStartButton.UseVisualStyleBackColor = true;
            this.saveVariablesAndStartButton.Click += new System.EventHandler(this.saveVariablesAndStartButton_Click);
            // 
            // numberOfSimulationMonthsInput
            // 
            this.numberOfSimulationMonthsInput.Location = new System.Drawing.Point(121, 139);
            this.numberOfSimulationMonthsInput.Maximum = new decimal(new int[] {
            2400,
            0,
            0,
            0});
            this.numberOfSimulationMonthsInput.Name = "numberOfSimulationMonthsInput";
            this.numberOfSimulationMonthsInput.Size = new System.Drawing.Size(120, 20);
            this.numberOfSimulationMonthsInput.TabIndex = 10;
            this.numberOfSimulationMonthsInput.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "#Simulation Months";
            // 
            // segregationPercentageInput
            // 
            this.segregationPercentageInput.DecimalPlaces = 1;
            this.segregationPercentageInput.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.segregationPercentageInput.Location = new System.Drawing.Point(122, 113);
            this.segregationPercentageInput.Name = "segregationPercentageInput";
            this.segregationPercentageInput.Size = new System.Drawing.Size(120, 20);
            this.segregationPercentageInput.TabIndex = 12;
            this.segregationPercentageInput.Value = new decimal(new int[] {
            93,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Segregation-percentage";
            // 
            // NewSimulationVariablesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 200);
            this.Controls.Add(this.segregationPercentageInput);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numberOfSimulationMonthsInput);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.saveVariablesAndStartButton);
            this.Controls.Add(this.vaccinePercentageInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.vaccineEffectivenessInput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numberOfDayCaresInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numberOfChildrenPerDayCareInput);
            this.Controls.Add(this.label1);
            this.Name = "NewSimulationVariablesForm";
            this.Text = "NewSimulationVariables";
            ((System.ComponentModel.ISupportInitialize)(this.numberOfChildrenPerDayCareInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfDayCaresInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vaccinePercentageInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vaccineEffectivenessInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfSimulationMonthsInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.segregationPercentageInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numberOfChildrenPerDayCareInput;
        private System.Windows.Forms.NumericUpDown numberOfDayCaresInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown vaccinePercentageInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown vaccineEffectivenessInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button saveVariablesAndStartButton;
        private System.Windows.Forms.NumericUpDown numberOfSimulationMonthsInput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown segregationPercentageInput;
        private System.Windows.Forms.Label label6;
    }
}