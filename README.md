# Project communiceren in de wiskunde

In deze repository is de broncode te vinden die wij gebruikt hebben voor ons onderzoek naar het effect van segregatie op de uitbraakgrootte mazelen.

De broncode van het C# programma is te vinden in de map `src/` en de geproduceerde data in `data/`. Verder staat in de map `python/` een script om de waarden van de variabelen 'contact intern' en 'contact extern' te bepalen. Ook staat in diezelfde map een script voor het bepalen van het betrouwbaarheidsinterval.
